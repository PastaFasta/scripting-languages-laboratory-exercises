$prefix = pop(@ARGV);
while(defined($line = <>)){
    $line =~ tr/A-Z/a-z/;
    @words = ($line =~ m/\b(\w{$prefix})/gi);
    foreach $word (@words){
        $dict{$word} += 1;
    }
}
@sorted = sort keys %dict;
foreach $word (@sorted){
    print("$word : $dict{$word}\n");
}