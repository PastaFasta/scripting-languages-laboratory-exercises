sub DateSpliter{
    #yyyy-mm-dd
    @date = split(/-/, $_[0]);
    return $date[2];
}

sub TimeSpliter{
    #hh:mm
    @time = split(/:/, $_[0]);
    return $time[0] * 3600 + $time[0] * 60 + $time[3];
}

sub Compare{
    @labDateAndTime = split(/ /, $_[0]);
    @sentDateAndTime = split(/ /, $_[1]);
    $dateDiff = DateSpliter($sentDateAndTime[0]) - DateSpliter($labDateAndTime[0]);
    $timeDiff = TimeSpliter($sentDateAndTime[1]) - TimeSpliter($labDateAndTime[1]);
    if($dateDiff <= 0 && $timeDiff < 3600){
        return 1;
    }
    return 0;
}

while(defined($line = <>)){
    @firstSplit = split(/;/, $line);
    $lastName = $firstSplit[1];
    @lab = split(/ /, $firstSplit[3]);
    if(!Compare($firstSplit[3], $firstSplit[4])){
        print("$firstSplit[0] $firstSplit[1] $firstSplit[2] - PROBLEM: $lab[0] $lab[1] -> $firstSplit[4]");
    }
}