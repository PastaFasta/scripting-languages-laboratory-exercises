$msg_counter = 0;
$currentLine = 1;
while(defined($line = <>)){
    if(!($line =~ /^From nobody/)){
        $dict{$msg_start_line} += 1;
    }else{
        $msg_start_line = $currentLine;
        $dict{$msg_start_line} = 0;
    }
    $currentLine++;
}
@sorted = sort {$dict{$b} <=> $dict{$a}} keys %dict;
foreach $mail (@sorted){
    if($msg_counter < 40){
        print("$mail : $dict{$mail}\n");
        $msg_counter++;
    }
}