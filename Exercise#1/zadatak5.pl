@coeffParts = split(/;/, <>);
while(defined($line = <>)){
    @students = split(/;/, $line);
    $i = 3;
    foreach $part (@coeffParts){
        if($students[$i] ne "-"){
            $students[$i] = $students[$i] * $part;
            $score += $students[$i];        
        }
        $i++;
    }
    $key = "$students[1], $students[2] ($students[0])";
    $hash{$key} = $score;
    $score = 0;
}
@studentsSorted = sort {$hash{$b} <=> $hash{$a}} keys %hash;
print("  Lista po rangu:\n----------------------------------------------\n");
$position = 1;
foreach $stud (@studentsSorted){
    printf("\t%3d. %-35s : %-.2f\n", $position, $stud, $hash{$stud});
    $position++;
}