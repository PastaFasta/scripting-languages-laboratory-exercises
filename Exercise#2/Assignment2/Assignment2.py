lines = open("ulaz.txt", "r").readlines()
print("Hyp#Q10#Q20#Q30#Q40#Q50#Q60#Q70#Q80#Q90")

iter = 1
for line in lines:
    line = list(line.strip().split(' '))
    l = len(line)
    for i in range(l):
        line[i] = float(line[i])
    line.sort()
    n = []
    print("%03d" % iter, end="")
    for i in (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9):
        num = int(round(i*l))
        n.append(line[num])
    for x in range(9):
        print('#'+str(n[x]), end="")
    print("")
    iter+=1