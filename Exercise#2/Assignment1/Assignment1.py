def getMatrix(number):
    rows = matrix[number].split('\n')
    roco = rows[0].split(' ')
    dict = {}
    dict['r'] = int(roco[0])         
    dict['c'] = int(roco[1])
    for row in rows[1:]:
        item = row.split(' ')
        dict[(int(item[0]), int(item[1]))] = float(item[2])

    return dict

def storeFile(matrix):
    dat = open("matrixOUT.txt", 'w')
    dat.write(str(matrix['r']) + ' ' + str(matrix['c']) + '\n')
    for item in matrix.keys():
        if item != 'r' and item != 'c':
            dat.write(str(item[0]) + ' ' + str(item[1]) + ' ' + str(matrix[item]) + '\n')
    dat.close()


def product(mat1, mat2):
    product = {}
    product['c'] = mat2['c']
    product['r'] = mat1['r']
    for i in range(1, mat1['r']+1):
        for j in range(1, mat2['c']+1):
                product[(i, j)] = 0
                for k in range(1, mat1['c']+1):
                    product[(i, j)] += float(mat1.get((i, k), 0) * mat2.get((k, j), 0))
                if product[(i, j)] == 0:
                    del product[(i, j)]
    return product

file = open("matrix.txt", 'r').read()
matrix = file.split("\n\n")

mat1 = getMatrix(0)
mat2 = getMatrix(1)

if mat1['c'] != mat2['r']:
    print("Matrice nisu ulancane")
    exit()


ma = product(mat1,mat2)
storeFile(ma)