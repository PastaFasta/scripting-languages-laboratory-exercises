import sys
import re
import urllib.request

def getLinks(links):
    print("Links:")
    for i in range(len(links)):
        links[i] = links[i].replace("href=", "")
        links[i] = links[i][1:-1]
        print("-", links[i])

def getHosts(links):
    for link in links:
        if "http://" in link:
            hosts = link.replace("http://", "")
            hosts = link.replace("www.", "")
            hosts = hosts.split("/")
            url = hosts[2]
            if host.get(url,0):
                host[url] += 1
            else:
                host[url] = 1
    print("\n  Hosts:")
    for key in host.keys():
        print(key, host[key])

def getMails(page):
    mails = re.findall(".+@.+\..+", page) #"[\w]+@[\w]+\.[\w]+"
    print("\n  Mails:")
    for mail in mails:
        print(mail.strip())

def getPictures(page):
    pic = re.findall('<img[\s]+src="[^"]+"[^>]+>', page)
    print("\n  Pictures:", len(pic))

arg = sys.argv[1]
page = urllib.request.urlopen(arg).read().decode("utf8")
links = re.findall('href="[^"]+"', page)
getLinks(links)
host = {}
getHosts(links)
getMails(page)
getPictures(page)