import os
import re

def getStudents():
    lines = open("studenti.txt", "r").readlines()
    students = {}
    for line in lines:
        info = line.strip().split(" ")
        students[info[0]] = {'lastN' : info[1], 'firstN' : info[2]}
        keys.append(info[0])
    return students

def getExercises(students):
    files = os.listdir(".")
    for file in files:
        if re.match('(Lab_[0-9]+_g[0-9]+.txt)', file):
            lab = open(file, "r").readlines()
            num = int(file.strip().split("_")[1])
            if  num not in exercises:
                exercises.append(num)
            for line in lab:
                data = line.strip().split(" ")
                if not students.get(line.strip().split(" ")[0],0).get(num,0):
                    students[line.strip().split(" ")[0]][num] = float(line.strip().split(" ")[1])
                else:
                    print("Student ", students[line.strip().split(" ")[0]]["lastN"], " zabiljezen vise od jednom na", str(num) , ". labosu")
    
    return students

def printStudentsLabPoints(students):
    numOfExercises = len(exercises)
    print("JMBAG       Prezime, Ime       ",end='')
    for lab in range(numOfExercises):
        print("     L" + str(lab+1),end='')
        
    print()
    for key in keys:
        print(key + "  " + "%-20s" % (students[key]["lastN"] + ", " + students[key]["firstN"]),end='')
        for j in range(1, numOfExercises+1):            
            print("    " + "%-3.1f" % (students.get(key,0).get(j,0)),end='')
        print()

keys = []
exercises = []
students = getStudents()
studs = getExercises(students)
printStudentsLabPoints(studs)