import sys

def readLog(page, log):
    lines = open(log, "r").readlines()
    for line in lines:
        ip = line.strip().split(' ')[0]
        subnet1 = ip.strip().split('.')[0]
        subnet2 = ip.strip().split('.')[1]
        subnet = subnet1 + "." + subnet2
        if subnet not in subNets.keys():
            subNets[subnet] = 1
        else:
            subNets[subnet] += 1

def printLogs():
    print("-------------------------------------------")
    print("Broj pristupa stranici: " + page)
    print("IP podmreza\t:\tBroj pristupa")
    print("-------------------------------------------")
    for subnet in sorted:
        if subNets[subnet] > 2:
            print(subnet, end='')
        print(".*.*", end='')
        print("\t:\t" + str(subNets[subnet]))

    print("-------------------------------------------")


if (len(sys.argv) < 2):
    print("Premalo ulaznih argumenata")
    exit()

page = sys.argv[1]
log = sys.argv[2]
subNets = {}
readLog(page, log)
sorted = sorted(subNets, key=subNets.__getitem__, reverse=True)
printLogs()
